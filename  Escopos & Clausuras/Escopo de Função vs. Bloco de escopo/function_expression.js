var a = 2;

// (function foo(){..})() como uma expressão significa que o identificador foo é encontrado somente no escopo onde o { .. } é indicado, não no escopo externo. 
// Escondendo o nome foo dentro dele mesmo significa que não polui o escopo envolvido desnecessariamente.

// (function(){..}()) é outra forma idêntica em termos de funcionalidade.
(function IIFE() {
	var a = 3; // 3
	console.log(a); // 3
})(); // a função é tratada como uma função de expressão (function-expression)

console.log(a); // 2

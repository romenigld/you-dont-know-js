function foo(str) {
	"use strict"; // significa que as declarações efetuadas dentro de eval() não modificam o escopo superior
	eval(str);
	console.log(a); // ReferenceError: a is not defined
}

foo( "var a = 2" );

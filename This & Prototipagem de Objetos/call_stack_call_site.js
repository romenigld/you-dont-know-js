function baz() {
  // call-stack é: `baz`
  // sendo assim, nosso call-site está no escopo global

  console.log( "baz" );
  bar(); // <-- call-site para `bar`
}

function bar () {
  // call-stack é: `baz` -> `bar`
  // sendo assim, nosso call-site está em `baz`

  console.log( "bar" );
  foo(); // <-- call-site para `foo`
}

function foo() {
  // call-stack é: `baz` -> `bar` -> `foo`
  // sendo assim, nosso call-site está em `bar`

  console.log( "foo" );
}

baz(); // <-- call-site para `baz`

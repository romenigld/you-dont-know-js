var myArray = [1, 2, 3];

for (var i = 0; i < myArray.length; i++) {
	console.log( myArray[i] );
}
// 1 2 3

for (var v in myArray) {
	console.log( v );
}
// 0
// 1
// 2

for (var v of myArray) {
	console.log( v );
}
// 1
// 2
// 3

// O laço for..of precisa de um objeto iterador (de uma função interna padrão conhecido 
// nas especificações como @@iterator) da coisa a ser iterada, e o laço então itera sobre
// sucessivos valores retornado da chamada do método next() do objeto iterador,
// uma vez para cada iteração do laço.
var myArray = [ 1, 2, 3 ];
var it = myArray[Symbol.iterator](); // @@iterator propriedade interna de um objeto usando Symbol do ES6: Symbol.iterator. 

it.next(); // { value:1, done:false }
it.next(); // { value:2, done:false }
it.next(); // { value:3, done:false }
it.next(); // { done:true }

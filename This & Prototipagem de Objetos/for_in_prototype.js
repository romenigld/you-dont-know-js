var anotherObject = {
	a: 2
};

// cria um objeto ligado ao `anotherObject`
var myObject = Object.create( anotherObject );

for (var k in myObject) {
	console.log("found: " + k);
}
// found: a

("a" in myObject); // retorna true
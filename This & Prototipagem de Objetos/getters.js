var myObject = {
	// define um getter para `a`
	get a() {
		return 2;
	}
};

myObject.a = 3;
myObject.a; // 2

Object.defineProperty(
	myObject,	// alvo
	"b",		// nome da propriedade
	{			// descritor
		// define um getter para `b`
		get: function(){ return this.a * 2 },

		// certifica que `b` aparece como uma propriedade do objeto
		enumerable: true
	}
);

myObject.a; // 2

myObject.b; // 4


var myObject = {
	// define um getter para `a`
	get a() {
		return this._a_;
	},

	// define um setter para `a`
	set a(val) {
		this._a_ = val * 2;
	}
};

myObject.a = 2;

myObject.a; // 4
var myObject = {
	a: 2
};

myObject.a;		// 2; Acesso à propriedade

myObject["a"];	// 2; Acesso à chave

var wantA = true;
var myObject = {
	a: 2
};

var idx;

if (wantA) {
	idx = "a";
}

console.log( myObject[idx] ); // 2

var myObject = { };

myObject[true] = "foo";
myObject[3] = "bar";
myObject[myObject] = "baz";

myObject["true"];				// "foo"
myObject["3"];					// "bar"
myObject["[object Object]"];	// "baz"

// ES6 adiciona nomes de propriedade computados,
// onde você pode especificar uma expressão, cercado por um par de [ ], 
// na posição do nome-da-chave de uma declaração de objeto-literal.
var prefix = "foo";

var myObject = {
	[prefix + "bar"]: "hello",
	[prefix + "baz"]: "world"
};

myObject["foobar"]; // hello
myObject["foobaz"]; // world

var myObject = {
	[Symbol.Something]: "hello world"
};

myObject[Symbol.Something]; // "hello world" 
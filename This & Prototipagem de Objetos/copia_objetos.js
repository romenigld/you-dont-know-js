function anotherFunction() { /*..*/ }

var anotherObject = {
	c: true
};

var anotherArray = [];

var myObject = {
	a: 2,
	b: anotherObject,	// referência, não uma cópia!
	c: anotherArray,	// outra referência!
	d: anotherFunction
};

anotherArray.push( anotherObject, myObject );

// objetos que são JSON-safe (que é, pode ser serializado para uma string JSON e 
//depois re-transformada em um objeto com a mesma estrutura e valores) podem facilmente ser duplicados com:
var newObj = JSON.parse( JSON.stringify( someObj ) );

// ES6 agora definiu Object.assign(..) para essa tarefa.
var newObj = Object.assign( {}, myObject );

newObj.a;						// 2
newObj.b === anotherObject;		// true
newObj.c === anotherArray;		// true
newObj.d === anotherFunction;	// true
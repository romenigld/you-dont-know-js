// "Classe Tradicional JS" `Vehicle`
function Vehicle() {
	this.engines = 1;
}
Vehicle.prototype.ignition = function() {
	console.log( "Turning on my engine." );
};
Vehicle.prototype.drive = function() {
	this.ignition();
	console.log( "Steering and moving forward!" );
};

// "Classe Parasita" `Car`
function Car() {
	// primeiramente, `car` é um `Vehicle`
	var car = new Vehicle();

	// agora, vamos modificar nosso `car` para especificá-lo
	car.wheels = 4;

	// salve uma referência especial para `Vehicle::drive()`
	var vehDrive = car.drive;

	// substitua `Vehicle::drive()`
	car.drive = function() {
		vehDrive.call( this );
		console.log( "Rolling on all " + this.wheels + " wheels!" );
	};

	return car;
}

var myCar = new Car();

myCar.drive();
// Turning on my engine.
// Steering and moving forward!
// Rolling on all 4 wheels!
// Forma declarativa ( Sintaxe Literal )
var myObj = {
  key: value
  //..
}

// Forma Construída 
var myObj = new Object();
myObj.key = value;
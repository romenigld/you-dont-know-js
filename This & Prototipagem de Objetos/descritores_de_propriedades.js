var myObject = {
	a: 2
};

Object.getOwnPropertyDescriptor( myObject, "a" );
// {
//    value: 2,
//    writable: true,
//    enumerable: true,
//    configurable: true
// }

// Object.defineProperty(..) para adicionar uma nova propriedade ou 
// modificar uma já existente (se ela for configurable!), com as características desejadas.
var myObject = {};

Object.defineProperty( myObject, "a", {
	value: 2,
	writable: true,
	configurable: true,
	enumerable: true
} );

myObject.a; // 2

// Gravável
// A habilidade de você mudar o valor de uma propriedade é controlada por writable.
// Considere:
var myObject = {};

Object.defineProperty( myObject, "a", {
	value: 2,
	writable: false, // não gravável!
	configurable: true,
	enumerable: true
} );

myObject.a = 3;

myObject.a; // 2

// em uso estricto se obtém um erro:
"use strict";

var myObject = {};

Object.defineProperty( myObject, "a", {
	value: 2,
	writable: false, // não gravável!
	configurable: true,
	enumerable: true
} );

myObject.a = 3; // TypeError

//Configurável
// Contanto que uma propriedade seja configurável, 
// podemos modificar sua definição de descritor usando o mesmo método defineProperty(..).
var myObject = {
	a: 2
};

myObject.a = 3;
myObject.a;					// 3

Object.defineProperty( myObject, "a", {
	value: 4,
	writable: true,
	configurable: false,	// não configurável!
	enumerable: true
} );

myObject.a;					// 4
myObject.a = 5;
myObject.a;					// 5

Object.defineProperty( myObject, "a", {
	value: 6,
	writable: true,
	configurable: true,
	enumerable: true
} ); // TypeError

// Obs.: Cuidado: como você pode ver, alterando configurable para false é uma ação de via única, e não pode ser desfeita!
// Nota: Há uma exceção diferenciada para estar ciente: mesmo se a propriedade já está com configurable:false, 
//writable sempre pode ser alterada de true para false sem erro, mas não o contrário, de false para true.

// Outra coisa que configurable:false previne é a habilidade de usar o operador delete para remover uma propriedade existente.
var myObject = {
	a: 2
};

myObject.a;				// 2
delete myObject.a;
myObject.a;				// undefined

Object.defineProperty( myObject, "a", {
	value: 2,
	writable: true,
	configurable: false,
	enumerable: true
} );

myObject.a;				// 2
delete myObject.a;
myObject.a;				// 2

// Constante de Objeto
var myObject = {};

Object.defineProperty( myObject, "FAVORITE_NUMBER", {
	value: 42,
	writable: false,
	configurable: false
} );

// Prevenir Extensões
var myObject = {
	a: 2
};

Object.preventExtensions( myObject );

myObject.b = 3;
myObject.b; // undefined
function foo() {
  // "use strict"; // Se utiliza irá gerar quando rodar foo()  TypeError: `this` is `undefined`
  console.log( this.a );
}

var a = 2;

(function() {
  "use strict"; // irrelevante
  foo(); 
})();

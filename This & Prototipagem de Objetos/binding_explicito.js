function foo() {
  console.log( this.a );
}

var obj = {
  a: 2
};

foo.call( obj ); // 2


function foo(something) {
  console.log( this.a, something );
  return this.a + something;
}

var obj = {
  a: 2
};

var bar = function() {
  return foo.apply( obj, arguments );
};

var b = bar( 3 ); // 2 3
console.log( b ); // 5

// Outra forma de representar esse padrão é criar um helper reutilizável:
function foo(something) {
  console.log( this.a, something );
  return this.a + something;
}

// `bind` helper simples
function bind(fn, obj) {
  return function() {
    return fn.apply( obj, arguments );
  };
}

var obj = {
  a: 2
};

var bar = bind( foo, obj );

var b = bar( 3 ); // 2 3
console.log( b ); // 5

// Já que hard binding é um padrão bem comum, é fornecido como uma utilidade nativa do ES5: Function.prototype.bind, e é usada assim:

function foo(something) {
  console.log( this.a, something );
  return this.a + something;
}

var obj = {
  a: 2
};

var bar = foo.bind( obj ); // bind(..) retorna uma nova função que é escrita para chamar a função original com o contexto do this definido como você especificou.

var b = bar( 3 ); // 2 3
console.log( b ); // 5

function foo() {
  console.log( this.a );
}

var obj = {
  a: 2,
  foo: foo
};

var bar = obj.foo; // referência para a função!

var a = "oops, global"; // `a` também é propriedade do objeto global

bar(); // "oops, global"

function foo() {
  console.log( this.a );
}

function doFoo(fn) {
  // `fn` é apenas mais uma referência para `foo`

  fn(); // <-- call-site!
}

var obj = {
  a: 2,
  foo: foo
};

var a = "oops, global"; // `a` também é propriedade do objeto global

doFoo( obj.foo ); // "oops, global"

function foo() {
  console.log( this.a );
}

var obj = {
  a: 2,
  foo: foo
};

var a = "oops, global"; // `a` também é propriedade do objeto global

setTimeout( obj.foo, 100 ); // "oops, global"
Pense sobre essa pseudo-implementação teórica e crua do setTimeout(), fornecido nativamente pelo ambiente JavaScript:

function setTimeout(fn,delay) {
  // espera (de alguma forma) por `delay` milissegundos
  fn(); // <-- call-site!
}

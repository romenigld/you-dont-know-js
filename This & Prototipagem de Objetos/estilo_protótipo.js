function Foo(name) {
	this.name = name;
}

Foo.prototype.myName = function() {
	return this.name;
};

function Bar(name,label) {
	Foo.call( this, name );
	this.label = label;
}

// aqui, nós criamos um novo `Bar.prototype`
// ligado à `Foo.prototype`
Bar.prototype = Object.create( Foo.prototype );

// Cuidado! Agora `Bar.prototype.constructor` se foi, 
// e pode precisar ser manualmente "consertado" se 
// você tiver o hábito de confiar em propriedades deste tipo!

Bar.prototype.myLabel = function() {
	return this.label;
};

var a = new Bar( "a", "obj a" );

a.myName(); // "a"
a.myLabel(); // "obj a"

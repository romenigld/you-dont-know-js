// Enumeração
var myObject = { };

Object.defineProperty(
	myObject,
	"a",
	// torne `a` enumarável normalmente
	{ enumerable: true, value: 2 }
);

Object.defineProperty(
	myObject,
	"b",
	// torne `b` não-enumerável
	{ enumerable: false, value: 3 }
);

myObject.b; // 3
("b" in myObject); // true
myObject.hasOwnProperty( "b" ); // true

myObject.propertyIsEnumerable( "a" ); // true
myObject.propertyIsEnumerable( "b" ); // false

Object.keys( myObject ); // ["a"]
Object.getOwnPropertyNames( myObject ); // ["a", "b"]

for (var k in myObject) {
	console.log( k, myObject[k] );
}
// "a" 2
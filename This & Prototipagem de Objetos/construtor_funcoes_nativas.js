var strPrimitive = "I am a string";
typeof strPrimitive;							// "string"
strPrimitive instanceof String;					// false

Object.prototype.toString.call( strPrimitive ); // [object String]

var strObject = new String( "I am a string" );
typeof strObject; 								// "object"
strObject instanceof String;					// true

// inspeciona o subtipo de 'object'
Object.prototype.toString.call( strObject );	// [object String]
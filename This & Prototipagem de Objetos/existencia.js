// Podemos perguntar a um objeto se ele possui certa propriedade sem pedir para obter o valor da propriedade:
var myObject = {
	a: 2
};

("a" in myObject);				// true
("b" in myObject);				// false

myObject.hasOwnProperty( "a" );	// true
myObject.hasOwnProperty( "b" );	// false
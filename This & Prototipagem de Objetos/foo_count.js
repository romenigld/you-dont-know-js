function foo(num) {
  console.log( "foo: " + num);
  
  this.count++;
}

foo.count = 0;

var i;

for (i = 0; i < 10; i++) {
  if (i > 5) {
    // using `call(..)`, we ensure the `this`
		// points at the function object (`foo`) itself
    foo.call( foo, i );
  }
}

console.log( foo.count);

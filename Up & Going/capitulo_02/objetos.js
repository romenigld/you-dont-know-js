var obj = {
	a: "hello world",
	b: 42,
	c: true
};

console.log(obj.a);
console.log(obj.b);
console.log(obj.c);

console.log(obj["a"]);
console.log(obj["b"]);
console.log(obj["c"]);

console.log("a notação em colchetes também é útil se você quiser acessar uma propriedade/chave onde o nome é armazenado dentro de outra variável, como por exemplo:");

var objeto = {
	a: "hello world",
	b: 42
};

var b = "a";

console.log(objeto[b]);
console.log(objeto["b"]);

